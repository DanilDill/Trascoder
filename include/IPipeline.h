#pragma once
#include <gst/gst.h>
#include <memory>
#include <string>

#include "IElement.h"
#include "IBin.h"

class IPipeline
{
public:
    IPipeline()=default;
    virtual ~IPipeline()=default;
    virtual void add(std::unique_ptr<IElement>&&)=0;
    virtual void add(std::unique_ptr<IBin>&&)=0;
    virtual void link()=0;
    virtual bool isNull()=0;
    virtual std::string getName()=0;
    virtual std::unique_ptr<GstElement, decltype(&gst_object_unref)>& toGstData()=0;
    virtual void run();
    virtual void pause();
};
