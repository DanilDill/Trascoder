#pragma once
#include <string>
struct Parameters
{
    std::string url;
    std::string outputPath;

    u_int segmentSecs;

    uint aspectRatioX;
    uint aspectRatioY;

    uint videoWidth;
    uint videoHeight;

    uint framerate;

    std::string decodeBy;
    std::string encodeBy;
    uint bframes;
    uint video_bitrate;
};
