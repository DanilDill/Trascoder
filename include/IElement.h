#pragma once
#include <gst/gst.h>
#include <memory>
#include <string>
class IElement
{
public:
    IElement()=default;
    virtual ~IElement()=default;
    virtual bool linkTo(const std::unique_ptr<IElement>&)=0;
    virtual bool isNull()=0;
    virtual std::unique_ptr<GstPad,decltype(&gst_object_unref)> getSrcPad()=0;
    virtual std::unique_ptr<GstPad,decltype(&gst_object_unref)> getSinkPad()=0;
    virtual std::string getName()=0;
    virtual std::unique_ptr<GstElement, decltype(&gst_object_unref)>& toGstData()=0;
};
