#pragma once
#include "IElement.h"
#include <vector>
class IBin
{
public:
    std::unique_ptr<IElement>& front();
    std::unique_ptr<IElement>& back();

    virtual bool link()=0;
    virtual bool linkTo(std::unique_ptr<IElement>& element)=0;
    virtual bool linkTo(std::unique_ptr<IBin>& bin)=0;
    virtual bool linkTo(std::unique_ptr<GstPad, decltype(&gst_object_unref)>&)=0;
    virtual bool add(std::unique_ptr<IElement>&&)=0;
    virtual bool add(std::unique_ptr<IBin>&&)=0;
    virtual std::unique_ptr<GstPad,decltype(&gst_object_unref)>& getSrcPad()=0;
    virtual std::unique_ptr<GstPad,decltype(&gst_object_unref)>& getSinkPad()=0;
    virtual ~IBin()=default;
    IBin():_bin(nullptr, nullptr),_name(), _items(){};
    virtual std::unique_ptr<GstBin, decltype(&gst_object_unref)>& toGstData()=0;
    friend class BinBase;

protected:
    std::string _name;
    std::unique_ptr<GstBin, decltype(&gst_object_unref)> _bin;
    std::vector<std::unique_ptr<IElement>> _items;
};