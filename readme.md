**Transcoder**    
Моя выпускная квалификационная работа в ВУЗ ТУСУР.

Реализованная программа выполняет компрессию\декомпрессию медиапотока, позволяет изменять основные параметры видео(разрешение,соотношение сторон,количество B-кадров), на выходе получеется сжатый медиапоток в формате hls-плейлиста

**Сборка**:

Для работы необходим Фреймворк Gstreamer. 

Установка из пакетов(Ubuntu 20 ):

     apt-get install libgstreamer1.0-dev  libgstrtspserver-1.0-dev gstreamer1.0-rtsp libgstreamer-plugins-base1.0-dev libgstreamer-plugins-bad1.0-dev gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly gstreamer1.0-libav gstreamer1.0-tools gstreamer1.0-x gstreamer1.0-alsa gstreamer1.0-gl gstreamer1.0-gtk3 gstreamer1.0-qt5 gstreamer1.0-pulseaudio

Использование Gstreamer-Cerbero для сборки(без запука cerbero-uninstalled shell):

        * cmake -DUSE_CERBERO=true -DCERBERO_ROOT_DIR=/path/to/cerbero Hset-env
        * make

**Использование GStreamer cerbero для отладки в Clion**
* Edit Configuration -> Environment Variables -> add from File.
* Указать файл doc/clion_environment.txt
* В файле clion_environment.txt переменной CERBERO_ROOT_PATH указать путь до корневой папке gstreamer-cerbero



