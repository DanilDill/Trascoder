#include <Input/InputDataStream.h>
void writeDotFile(GstElement *pipeline, std::string name)
{
    GST_DEBUG_BIN_TO_DOT_FILE( GST_BIN( pipeline),GST_DEBUG_GRAPH_SHOW_ALL, name.c_str() );
}

void CreateBasicTest()
{
    GstElement* pipe = gst_pipeline_new("CreateBasicTestWithUrl_test");
    std::unique_ptr<IBin> bin = std::unique_ptr<InputDataStream>(new InputDataStream());
    BinBase::add_to_pipeline(pipe,bin);
    bin->link();
    writeDotFile(pipe, "BasicCreateInputStream");

    gst_object_unref(pipe);
}
void CreateBasicTestWithUrl()
{
    GstElement* pipe = gst_pipeline_new("CreateBasicTestWithUrl_test");
    std::unique_ptr<IBin> bin = std::unique_ptr<InputDataStream>(new InputDataStream("http://127.0.0.1:8443"));
    BinBase::add_to_pipeline(pipe,bin);
    bin->link();
    writeDotFile(pipe, "CreateBasicTestWithUrl");

    gst_object_unref(pipe);
}
int main (int argc, char *argv[])
{
    bool success = g_setenv ("GST_DEBUG_DUMP_DOT_DIR",
                             "/home/danil/Trascoder/dot/",
                             true);
    gst_init (&argc, &argv);
    CreateBasicTest();
    CreateBasicTestWithUrl();
    return 0;
}