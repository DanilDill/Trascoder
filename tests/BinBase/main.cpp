#include <Base/BinBase/BinBase.h>
#include <Base/ElemenBase/ElementBase.h>
void writeDotFile(GstElement *pipeline, std::string name)
{
    GST_DEBUG_BIN_TO_DOT_FILE( GST_BIN( pipeline),GST_DEBUG_GRAPH_SHOW_ALL, name.c_str() );
}

void BinCreateBaseTest()
{
    GstElement* pipe = gst_pipeline_new("BinCreateTest");
    std::unique_ptr<IBin> bin = std::unique_ptr<BinBase>(new BinBase("test_bin"));
    std::unique_ptr<IElement> element1 = std::unique_ptr<ElementBase>(new ElementBase("testsrc","videotestsrc"));
    std::unique_ptr<IElement> element2 = std::unique_ptr<ElementBase>(new ElementBase("convert","videoconvert"));
    bin->add(std::move(element1));
    bin->add(std::move(element2));
    bin->link();
    BinBase::add_to_pipeline(pipe,bin);
    writeDotFile(pipe, "BinCreateTest");
    gst_object_unref(pipe);
}


void BinLinkToElementsTest()
{
    GstElement* pipe = gst_pipeline_new("BinLinkToElementsTest");
    std::unique_ptr<IBin> bin = std::unique_ptr<BinBase>(new BinBase("test_bin"));
    std::unique_ptr<IElement> element1 = std::unique_ptr<ElementBase>(new ElementBase("testsrc","videotestsrc"));
    std::unique_ptr<IElement> element2 = std::unique_ptr<ElementBase>(new ElementBase("convert","videoconvert"));
    bin->add(std::move(element1));
    bin->add(std::move(element2));
    bin->link();
    BinBase::add_to_pipeline(pipe,bin);
    std::unique_ptr<IElement> element3 = std::unique_ptr<ElementBase>(new ElementBase("fakesink","fakesink") );
    gst_bin_add (GST_BIN (pipe), GST_ELEMENT(element3->toGstData().get()));
    bin->linkTo(element3);
    writeDotFile(pipe, "BinLinkToElementsTest");
    gst_object_unref(pipe);
}

void BinLinkToBinTest()
{
    GstElement* pipe = gst_pipeline_new("BinLinkToBinTest");
    std::unique_ptr<IBin> bin = std::unique_ptr<BinBase>(new BinBase("test_bin"));
    std::unique_ptr<IElement> element1 = std::unique_ptr<ElementBase>(new ElementBase("testsrc","videotestsrc"));
    std::unique_ptr<IElement> element2 = std::unique_ptr<ElementBase>(new ElementBase("convert","videoconvert"));
    bin->add(std::move(element1));
    bin->add(std::move(element2));
    bin->link();
    BinBase::add_to_pipeline(pipe,bin);

    std::unique_ptr<IBin> bin2 = std::unique_ptr<BinBase>(new BinBase("test_bin2"));
    std::unique_ptr<IElement> element3 = std::unique_ptr<ElementBase>(new ElementBase("queue","queue") );
    std::unique_ptr<IElement> element4 = std::unique_ptr<ElementBase>(new ElementBase("fakesink","fakesink"));
    bin2->add(std::move(element3));
    bin2->add(std::move(element4));
    bin2->link();
    BinBase::add_to_pipeline(pipe,bin2);
    writeDotFile(pipe, "BinLinkToBinTest_AddingPipeline");
    bin->linkTo(bin2);
    BinBase::add_to_pipeline(pipe,bin2);
    writeDotFile(pipe, "BinLinkToBinTest");
    gst_object_unref(pipe);
}
void NestedBinCreateTest()
{
    GstElement* pipe = gst_pipeline_new("BinCreateTest");
    std::unique_ptr<IBin> bin = std::unique_ptr<BinBase>(new BinBase("test_bin"));
    std::unique_ptr<IElement> element1 = std::unique_ptr<ElementBase>(new ElementBase("testsrc","videotestsrc"));
    std::unique_ptr<IElement> element2 = std::unique_ptr<ElementBase>(new ElementBase("convert","videoconvert"));
    bin->add(std::move(element1));

    std::unique_ptr<IBin> bin2 = std::unique_ptr<BinBase>(new BinBase("test_bin_2"));
    std::unique_ptr<IElement> element3 = std::unique_ptr<ElementBase>(new ElementBase("scale","videoscale") );
    std::unique_ptr<IElement> element4 = std::unique_ptr<ElementBase>(new ElementBase("rate","videorate"));
    bin2->add(std::move(element3));
    bin2->add(std::move(element4));
    bin2->link();

    bin->add(std::move(bin2));
    bin->link();
    BinBase::add_to_pipeline(pipe,bin);
    writeDotFile(pipe, "NestedBinCreateTest");
    gst_object_unref(pipe);
}
int main (int argc, char *argv[])
{
    bool success = g_setenv ("GST_DEBUG_DUMP_DOT_DIR",
                             "/home/danil/Trascoder/dot/",
                             true);
    gst_init (&argc, &argv);
    BinCreateBaseTest();
    BinLinkToElementsTest();
    BinLinkToBinTest();
    NestedBinCreateTest();



    return 0;
}