#include "Audio/Audio.h"

void writeDotFile(GstElement *pipeline, std::string name)
{
    GST_DEBUG_BIN_TO_DOT_FILE( GST_BIN( pipeline),GST_DEBUG_GRAPH_SHOW_ALL, name.c_str() );
}


void AudioDecodeBinCreateTest()
{
    GstElement* pipe = gst_pipeline_new("Basic_Create_Audio_Bins");
    auto au_decodebin = Audio::Builder::makeDecoderBin();
    auto au_pp = Audio::Builder::makePostProcerssorBin();
    auto au_encode_bin = Audio::Builder::makeEncoderBin();
    BinBase::add_to_pipeline(pipe,au_decodebin);
    BinBase::add_to_pipeline(pipe,au_pp);
    BinBase::add_to_pipeline(pipe,au_encode_bin);
    writeDotFile(pipe,"Basic_Create_Audio_Bins");
}

int main (int argc, char *argv[])
{
    bool success = g_setenv ("GST_DEBUG_DUMP_DOT_DIR",
                             "/home/danil/Trascoder/dot/",
                             true);
    gst_init (&argc, &argv);
    AudioDecodeBinCreateTest();
    return 0;
}