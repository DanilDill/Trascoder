
#include <Base/ElemenBase/ElementBase.h>
void writeDotFile(GstElement *pipeline, std::string name)
{
    GST_DEBUG_BIN_TO_DOT_FILE( GST_BIN( pipeline),GST_DEBUG_GRAPH_SHOW_ALL, name.c_str() );
}
int main (int argc, char *argv[])
{
    bool success = g_setenv ("GST_DEBUG_DUMP_DOT_DIR",
                             "/home/danil/Trascoder/dot/",
                             true);
    gst_init (&argc, &argv);

    GstElement* pipe = gst_pipeline_new("ElementBase_test");
    std::unique_ptr<IElement> element1 = std::unique_ptr<ElementBase>(new ElementBase("testsrc","videotestsrc"));
    std::unique_ptr<IElement> element2 = std::unique_ptr<ElementBase>(new ElementBase("videoconvert","videoconvert"));
    std::unique_ptr<IElement> element3 = std::unique_ptr<ElementBase>(new ElementBase("fakesink","fakesink"));
    gst_bin_add (GST_BIN (pipe), GST_ELEMENT(element1->toGstData().get()));
    gst_bin_add (GST_BIN (pipe), GST_ELEMENT(element2->toGstData().get()));
    gst_bin_add (GST_BIN (pipe), GST_ELEMENT(element3->toGstData().get()));
    element1->linkTo(element2);
    element2->linkTo(element3);
    writeDotFile(pipe, "adding_elem");
    gst_object_unref(pipe);
    return 0;
}