#pragma once
#include <gst/gst.h>
#include <string>
#include <Base/BinBase/BinBase.h>
class  InputDataStream : public BinBase
{
public:
    InputDataStream();
    InputDataStream(const std::string& url);

    static void  onDemuxPadAdded(GstElement *element, GstPad *new_pad, gpointer  data);

private:
    void setURL(const std::string& url);
    static void pad_added_handlerV(GstElement* src, GstPad* new_pad, gpointer & data)
    {
        GstPad* sinkpad;
        GstElement* parser = (GstElement*)data;
        g_print("Dynamic pad created, linking mDemuxer/Vparser\n");
        sinkpad = gst_element_get_static_pad(parser, "sink");
        gst_pad_link(new_pad, sinkpad);
        gst_object_unref(sinkpad);

    }
    void pad_added_handlerA(GstElement* src, GstPad* new_pad, gpointer& data)
    {
        GstPad* sinkpad2;
        GstElement* decoder2 = (GstElement*)data;
        g_print("Dynamic pad created, linking mDemuxer/aqueue\n");
        sinkpad2 = gst_element_get_static_pad(decoder2, "sink");
        gst_pad_link(new_pad, sinkpad2);
        gst_object_unref(sinkpad2);
    }
private:


    std::string mUrl;

};


