
#include "InputDataStream.h"
#include <Base/ElemenBase/ElementBase.h>

InputDataStream::InputDataStream(): BinBase("input_data_stream")
{
    add(std::move(std::unique_ptr<ElementBase>(new ElementBase("media_source","souphttpsrc"))));
    add(std::move(std::unique_ptr<ElementBase>(new ElementBase("media_demultiplexer","tsdemux"))));
};

InputDataStream::InputDataStream(const std::string& url) : InputDataStream()
{
    mUrl = url;
    setURL(mUrl);
}

void InputDataStream::setURL(const std::string& url)
{
    g_object_set(GST_OBJECT(front().get()), "location", url.c_str(), nullptr);
}

void InputDataStream::onDemuxPadAdded(GstElement *element, GstPad *new_pad, gpointer  data)
{
    GstPad* sinkpad;
    GstElement* decoder = (GstElement*)data;
    g_print("Dynamic pad created, linking mDemuxer/decoder\n");
    sinkpad = gst_element_get_static_pad(decoder, "sink");
    gst_pad_link(new_pad, sinkpad);
    gst_object_unref(sinkpad);

}



