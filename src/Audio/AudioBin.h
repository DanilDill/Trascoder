#pragma once
#include "Base/BinBase/BinBase.h"


class AudioDecodeBin : public BinBase
{
public:
    AudioDecodeBin();
    static void PadAddedCallback(GstElement *element, GstPad *pad, gpointer data);
};

class AudioEncodeBin : public BinBase
{
public:
    AudioEncodeBin();
};

class AudioPostProcessingBin: public BinBase
{
public:
    AudioPostProcessingBin();
};

