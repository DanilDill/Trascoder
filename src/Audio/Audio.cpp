#include "Audio.h"


namespace Audio
{
    std::unique_ptr<IBin> Builder::makeDecoderBin()
    {
        return std::unique_ptr<AudioDecodeBin>(new AudioDecodeBin);
    }

    std::unique_ptr<IBin> Builder::makePostProcerssorBin()
    {
        return std::unique_ptr<AudioPostProcessingBin>(new AudioPostProcessingBin);
    }

    std::unique_ptr<IBin> Builder::makeEncoderBin()
    {
        return std::unique_ptr<AudioEncodeBin>(new AudioEncodeBin);
    }
}
