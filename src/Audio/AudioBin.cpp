#include "AudioBin.h"
#include "Base/ElemenBase/ElementBase.h"

using audio_element = std::unique_ptr<ElementBase>;
AudioDecodeBin::AudioDecodeBin(): BinBase("audio_decodebin")
{
    add(audio_element(new ElementBase("audio_decodebin_queue","queue")));
    add(audio_element(new ElementBase("audio_decodebin_decoder","decodebin")));
    link();
    g_object_set(front()->toGstData().get(),"max-size-time", 100000000000, NULL);

}

void AudioDecodeBin::PadAddedCallback(GstElement *element, GstPad *pad, gpointer data)
{
    gchar *name;
    GstElement *other = (GstElement*)data;

    name = gst_pad_get_name (pad);
    g_print ("A new pad %s was created for %s\n", name, gst_element_get_name(element));
    g_free (name);

    g_print ("element %s will be linked to %s\n",
             gst_element_get_name(element),
             gst_element_get_name(other));
    gst_element_link(element, other);
};




AudioPostProcessingBin::AudioPostProcessingBin(): BinBase("Audio_PostProcessing_Bin")
{
    add(audio_element(new ElementBase("audio_convert","audioconvert")));
    add(audio_element(new ElementBase("audio_resample","audioresample")));
    link();
}


AudioEncodeBin::AudioEncodeBin(): BinBase("audio_encode_bin")
{
    add(audio_element(new ElementBase("audio_encoder","avenc_aac")));
    add(audio_element(new ElementBase("audio_resample","aacparse")));
    link();

}
