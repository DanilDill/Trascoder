#pragma once
#include "AudioBin.h"
namespace Audio
{
    class Builder
    {
    public:
        static std::unique_ptr<IBin> makeDecoderBin();
        static std::unique_ptr<IBin> makePostProcerssorBin();
        static std::unique_ptr<IBin> makeEncoderBin();
    };
}