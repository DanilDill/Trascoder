
#include "FileSinkBin.h"
#include "Base/ElemenBase/ElementBase.h"
FileSinkBin::FileSinkBin(): BinBase("FileSinkBin")
{
    add(std::unique_ptr<IElement>(new ElementBase("tsMuxes","mpegtsmux")));
    add(std::unique_ptr<IElement>(new ElementBase("sink","hlssink")));
}

/*
 * TODO: придумать как задавать параметры
FileSinkBin::FileSinkBin(const Parameters& parameters) : FileSinkBin()
{
    std::string playlistPath = parameters.outputPath + "/playlist.m3u8";
    std::string filespath = parameters.outputPath + "/segment%05d.ts";
    g_object_set(_hlsSink.get(), "playlist-location", playlistPath.c_str(), NULL);
    g_object_set(_hlsSink.get(), "location", filespath.c_str(), NULL);
    g_object_set(_hlsSink.get(), "playlist-length", 0, NULL);
    g_object_set(_hlsSink.get(), "max-files", 4294967295, NULL);
    g_object_set(_hlsSink.get(), "target-duration", 5, NULL);
}*/
