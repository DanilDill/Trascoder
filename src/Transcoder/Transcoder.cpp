#include "Transcoder.h"
//#include "DataStructure.h"
/*void writeDotFile( GstBin  *bin , const std::string& filename)
{

    gst_debug_bin_to_dot_file(bin, GST_DEBUG_GRAPH_SHOW_ALL, filename.c_str());

}*/
/* Initialize GStreamer */
Transcoder::Transcoder(int argc, char* argv[],const Parameters& parameters):
data(parameters)
{
    setParameters(parameters);
    if (!data.isNotNull()) {
        g_printerr("Not all elements could be created.\n");
        _terminate = true;
        return;
    }

    data.addMany();
    if (!data.linkMany()) {
        g_printerr("Elements could not be linked.\n");
        gst_object_unref(data._pipeline);
        _terminate = true;
    }
}

void Transcoder::setParameters(const Parameters& parameters)
{
    mParameters = parameters;
}

/* Set the URI to play */
void Transcoder::prepare()
{
    data.LinkRequestPads();
    g_signal_connect(data._inputStream.mDemuxer, "pad-added", G_CALLBACK(Transcoder::pad_added_handlerV), &data);
    g_signal_connect(data._inputStream.mDemuxer, "pad-added", G_CALLBACK(Transcoder::pad_added_handlerA), &data);
    g_signal_connect(data._audioDecodeBin.decodebin.get(), "pad-added", G_CALLBACK(Transcoder::pad_added_handlerAD), &data);
}

int Transcoder::startPlaying()
{
    GstBus* bus;
    GstMessage* msg;
    GstStateChangeReturn ret;
    gboolean terminate = false;

    /* Start playing */
    ret = gst_element_set_state( data._pipeline, GST_STATE_PLAYING );
    data.writeDotFile( "START" );
    if ( ret == GST_STATE_CHANGE_FAILURE )
    {
        g_printerr( "Unable to set the pipeline to the playing state.\n" );
        data.writeDotFile( "UnablePLayState" );
        gst_object_unref( data._pipeline );
        return -1;
    }

    /* Listen to the bus */
    bus = gst_element_get_bus( data._pipeline );

    do
    {
        msg = gst_bus_timed_pop_filtered( bus, GST_CLOCK_TIME_NONE,
            static_cast<GstMessageType>( GST_MESSAGE_STATE_CHANGED | GST_MESSAGE_ERROR | GST_MESSAGE_EOS ) );

        /* Parse message */
        if ( msg != NULL )
        {
            GError* err;
            gchar* debug_info;
            switch (GST_MESSAGE_TYPE(msg))
            {
            case GST_MESSAGE_ERROR:
                gst_message_parse_error( msg, &err, &debug_info );
                g_printerr( "Error received from element %s: %s\n", GST_OBJECT_NAME( msg->src ), err->message );
                g_printerr("Debugging information: %s\n", debug_info ? debug_info : "none");
                g_clear_error(&err);
                g_free(debug_info);
                GST_DEBUG_BIN_TO_DOT_FILE( GST_BIN( data._pipeline ), GST_DEBUG_GRAPH_SHOW_ALL, "error" );
                terminate = true;
                break;
            case GST_MESSAGE_EOS:
                g_print( "End-Of-Stream reached.\n" );
                    GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN( data._pipeline ), GST_DEBUG_GRAPH_SHOW_ALL,"EOS" );
                terminate = true;
                break;
            case GST_MESSAGE_STATE_CHANGED:
                /* We are only interested in state-changed messages from the pipeline */
                if (GST_MESSAGE_SRC(msg) == GST_OBJECT( data._pipeline ) )
                {
                    GstState old_state, new_state, pending_state;
                    gst_message_parse_state_changed( msg, &old_state, &new_state, &pending_state );
                    g_print("Pipeline state changed from %s to %s:\n",
                        gst_element_state_get_name( old_state ), gst_element_state_get_name( new_state ) );
                    data.writeDotFile( "Changed" );
                }
                break;
            default:
                g_printerr( "Unexpected message received.\n" );
                break;
            }
            gst_message_unref( msg );
        }
    } while ( !terminate );

    /* Free resources */
    gst_object_unref(bus);
    gst_element_set_state(data._pipeline, GST_STATE_NULL);
    gst_object_unref(data._pipeline);
    return 0;
}

bool Transcoder::GstGetTerminate()
{
    return _terminate;
}


/* This function will be called by the pad-added signal */
void Transcoder::pad_added_handlerV(GstElement* src, GstPad* new_pad, Pipeline& data)
{
    GstPad* sinkpad;
    GstElement* parser = (GstElement*)data._videoDecodeBin.first().get();
    g_print("Dynamic pad created, linking mDemuxer/Vparser\n");
    sinkpad = gst_element_get_static_pad(parser, "sink");
    gst_pad_link(new_pad, sinkpad);
    gst_object_unref(sinkpad);

}
void Transcoder::pad_added_handlerA(GstElement* src, GstPad* new_pad, Pipeline& data)
{
    GstPad* sinkpad2;
    GstElement* decoder2 = (GstElement*)data._audioDecodeBin.audioQueue.get();
    g_print("Dynamic pad created, linking mDemuxer/aqueue\n");
    sinkpad2 = gst_element_get_static_pad(decoder2, "sink");
    gst_pad_link(new_pad, sinkpad2);
    gst_object_unref(sinkpad2);
}



 void Transcoder::pad_added_handlerAD(GstElement* src, GstPad* new_pad, Pipeline& data)
{

    GstPad* sink_pad = gst_element_get_static_pad(data._audioPostProcessingBin.audioconvert, "sink");
    GstPadLinkReturn ret;
    GstCaps* new_pad_caps = NULL;
    GstStructure* new_pad_struct = NULL;
    const gchar* new_pad_type = NULL;

    g_print("Received new pad '%s' from '%s':\n", GST_PAD_NAME(new_pad), GST_ELEMENT_NAME(src));

    if (gst_pad_is_linked(sink_pad)) {
        g_print("We are already linked. Ignoring.\n");
        goto exit;
    }

    new_pad_caps = gst_pad_get_current_caps(new_pad);
    new_pad_struct = gst_caps_get_structure(new_pad_caps, 0);
    new_pad_type = gst_structure_get_name(new_pad_struct);
    ret = gst_pad_link(new_pad, sink_pad);

    if ( !g_str_has_prefix(new_pad_type, "audio/x-raw") )
    {
        g_print( "It has type '%s' which is not raw audio. Ignoring.\n", new_pad_type );
        goto exit;
    }

    ret = gst_pad_link(new_pad, sink_pad);
    if (GST_PAD_LINK_FAILED(ret))
    {
        g_print("Type is '%s' but link failed.\n", new_pad_type);
    }
    else
    {
        g_print("Link succeeded (type '%s').\n", new_pad_type);
    }

exit:
    if (new_pad_caps != NULL)
    {
        gst_caps_unref(new_pad_caps);
    }
    gst_object_unref(sink_pad);
}



