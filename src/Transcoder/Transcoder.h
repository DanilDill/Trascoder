#pragma once
#include <gst/gst.h>

#include <Pipeline/Pipeline.h>
#include <include/Parameters.h>
class Transcoder
{
private:
    Pipeline data;
    bool _terminate = false;


public:

    Transcoder(int argc, char* argv[],const Parameters& parameters);

    /* Set the URI to play */
    void prepare();

    void setParameters(const Parameters& parameters);
    int startPlaying();

    bool GstGetTerminate();

private:
    /* This function will be called by the pad-added signal */
    Parameters mParameters;
    static void pad_added_handlerV(GstElement* src, GstPad* new_pad, Pipeline& data);
    static void pad_added_handlerA(GstElement* src, GstPad* new_pad, Pipeline& data);
    static void pad_added_handlerAD(GstElement* src, GstPad* new_pad, Pipeline& data);
  //  bool GstReadyElements();
};

