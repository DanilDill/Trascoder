#pragma once
#include "VideoBin.h"

namespace Video
{
    class Builder
    {
    public:
        static  std::unique_ptr<IBin> makeCpuDecoderBin() { return std::unique_ptr<IBin>(new CpuDecoderBin); };
        static  std::unique_ptr<IBin> makeCpuVppBin(){ return std::unique_ptr<IBin>(new CpuVppBin); };
        static  std::unique_ptr<IBin> makeCpuEncodeBin(){ return std::unique_ptr<IBin>(new CpuEncoderBin); };

        static  std::unique_ptr<IBin> makeGpuDecoderBin(){ return std::unique_ptr<IBin> (new GpuDecoderBin); };
        static  std::unique_ptr<IBin> makeGpuVppBin(){ return std::unique_ptr<IBin>(new GpuVppBin); };
        static  std::unique_ptr<IBin> makeGpuEncodeBin(){ return std::unique_ptr<IBin> (new GpuEncoderBin);};

    };
}