#pragma once
#include <Base/BinBase/BinBase.h>
#include <Base/ElemenBase/ElementBase.h>
using element = std::unique_ptr<ElementBase>;

class CpuDecoderBin: public BinBase
{
public:
    CpuDecoderBin() : BinBase("cpu_decoder_bin")
    {
        add(element(new ElementBase("video_decodebin_queue","queue")));
        add(element(new ElementBase("video_decodebin_h264parser","h264parse")));
        add(element(new ElementBase("video_decodebin_decoder","avdec_h264")));
        link();
    };
};

class GpuDecoderBin: public BinBase
{
public:
    GpuDecoderBin() : BinBase("nvidia_decoder_bin")
    {
        add(element(new ElementBase("video_decodebin_queue","queue")));
        add(element(new ElementBase("video_decodebin_h264parser","h264parse")));
        add(element(new ElementBase("video_decodebin_decoder","nvh264dec")));
        link();
    };
};


class CpuVppBin : public BinBase
{
public:
    CpuVppBin(): BinBase("cpu_vpp_bin")
    {
        add(element(new ElementBase("vpp_cpu_convert","videoconvert")));
        add(element(new ElementBase("vpp_cpu_scale","videoscale")));
        add(element(new ElementBase("vpp_cpu_fps","videorate")));
        add(element(new ElementBase("vpp_cpu_aspect","aspectratiocrop")));
        add(element(new ElementBase("vpp_cpu_capssetter","capsetter")));
        link();
    };
};


class GpuVppBin :  public BinBase
{
public:
    GpuVppBin(): BinBase("nvidia_gpu_bin")
    {
        add(element(new ElementBase("vpp_gpu_convert","cudaconvert")));
        add(element(new ElementBase("vpp_gpu_scale","cudascale")));
        add(element(new ElementBase("vpp_gpu_fps","videorate")));
        add(element(new ElementBase("vpp_gpu_aspect","aspectratiocrop")));
        add(element(new ElementBase("vpp_gpu_capssetter","capsetter")));
        link();
    };

};


class CpuEncoderBin : public BinBase
{
public:
    CpuEncoderBin(): BinBase("cpu_encoder_bin")
    {
        add(element(new ElementBase("encoder_cpu_x264","x264enc")));
        add(element(new ElementBase("encoder_cpu_parser","h264parse")));
        link();
    };
};

class GpuEncoderBin : public BinBase
{
public:
    GpuEncoderBin(): BinBase("gpu_encoder_bin")
    {
        add(element(new ElementBase("encoder_gpu_h264","x264enc")));
        add(element(new ElementBase("encoder_gpu_parser","h264parse")));
        link();
    };
};


