#pragma once
#include <include/IElement.h>
class ElementBase : public IElement
{
public:
    using PadPtr = std::unique_ptr<GstPad,decltype(&gst_object_unref)>;
    using ElementPtr = std::unique_ptr<GstElement, decltype(&gst_object_unref)>;

    ElementBase(const std::string& name, const std::string& id);
    ElementBase(const std::string& name,const std::string& id, ElementPtr&& element);
    virtual bool linkTo(const std::unique_ptr<IElement>& other) override;
    virtual bool isNull() override;
    virtual inline std::string getName() override;
    virtual ElementPtr& toGstData() override;
    static std::unique_ptr<IElement> makeFrom(ElementPtr&&);
    virtual PadPtr getSrcPad() override;
    virtual PadPtr getSinkPad() override;
protected:
    ElementPtr _element;
    std::string _name;
    std::string _id;

};
