#include "ElementBase.h"

ElementBase::ElementBase(const std::string& name,const std::string& id, ElementPtr&& element):
IElement(),_name(name),_id(id),_element(std::move(element))
{
}

ElementBase::ElementBase(const std::string& name, const std::string& id):
ElementBase(name,id,ElementPtr(gst_element_factory_make(id.data(),name.data()),&gst_object_unref))
{
}

std::unique_ptr<GstElement, decltype(&gst_object_unref)>& ElementBase::toGstData()
{
    return _element;
}

inline std::string ElementBase::getName()
{
    return _name;
}

 std::unique_ptr<GstPad,decltype(&gst_object_unref)> ElementBase::getSrcPad()
{
    return  PadPtr(gst_element_get_static_pad(toGstData().get(),"src"), &gst_object_unref);
}

std::unique_ptr<GstPad,decltype(&gst_object_unref)> ElementBase::getSinkPad()
{
    auto pad =  PadPtr(gst_element_get_static_pad(toGstData().get(),"sink"), &gst_object_unref);
    return  pad;
}

std::unique_ptr<IElement> ElementBase::makeFrom(ElementPtr&& gstElement)
{
    auto element = (std::unique_ptr<ElementBase>(new ElementBase("","", std::move(gstElement))));
    return element;
}

 bool ElementBase::linkTo(const std::unique_ptr<IElement> &other)
 {
     return  gst_element_link_many( _element.get(),other->toGstData().get(), nullptr );
 }

bool ElementBase::isNull()
{
    return _element == nullptr;
}
