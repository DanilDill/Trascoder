project(BinBase)
message(STATUS "    building ${PROJECT_NAME} Library")


file(GLOB include ${CMAKE_CURRENT_SOURCE_DIR}/*.h)
file(GLOB sources ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp)

add_library(${PROJECT_NAME} ${include} ${sources} )
include_directories(interfaces)
target_include_directories(${PROJECT_NAME} PUBLIC ../..)
target_link_libraries(${PROJECT_NAME}
        interfaces
        ElementBase
        ${GLOB_LIBRARIES}
        ${GSTREAMER_LIBRARIES}
        ${GSTREAMER_BASE_LIBRARIES}
        ${GSTREAMER_RTSP_LIBRARIES}
        ${GSTREAMER_RTSPSERVER_LIBRARIES}
        ${GSTREAMER_VIDEO_LIBRARIES}
        )