#include <include/IBin.h>

using PadPtr = std::unique_ptr<GstPad,decltype(&gst_object_unref)>;
using Element = std::unique_ptr<IElement>;
using GstElementPtr = std::unique_ptr<GstElement, decltype(&gst_object_unref)>;
using BinPtr = std::unique_ptr<GstBin, decltype(&gst_object_unref)>;

class BinBase : public IBin
{
public:
    enum class PadType
    {
        PAD_SRC,
        PAD_SINK
    };
public:
    BinBase(const std::string& name);
    static  GstElementPtr toGstElementPtr(std::unique_ptr<IBin>&);
    virtual bool link() override;
    virtual bool linkTo(Element& element)override;
    virtual bool linkTo(std::unique_ptr<IBin>& bin)override;
    virtual bool linkTo(PadPtr& pad);
    virtual bool add(std::unique_ptr<IElement>&&) override;
    virtual bool add(std::unique_ptr<IBin>&&) override;
    virtual std::unique_ptr<GstPad,decltype(&gst_object_unref)>& getSrcPad()override;
    virtual std::unique_ptr<GstPad,decltype(&gst_object_unref)>& getSinkPad()override;
    virtual bool makePad(PadType type);
    Element& front();
    Element& back();
    static GstElementPtr toGstElement(BinPtr&& bin);
    virtual BinPtr& toGstData() override;
    //TODO only for testing
    static void add_to_pipeline(GstElement* pipeline,const std::unique_ptr<IBin>& bin);

protected:
    PadPtr _padSrc;
    PadPtr _padSink;
};