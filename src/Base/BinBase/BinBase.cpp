#include "BinBase.h"
#include <Base/ElemenBase/ElementBase.h>
#include <iostream>
#include <exception>
BinBase::BinBase(const std::string& name): IBin(),
_padSink(nullptr, nullptr), _padSrc(nullptr, nullptr)
{
    _name = name;
    _bin = BinPtr( (GstBin*) gst_bin_new (_name.data()), &gst_object_unref);

}

bool BinBase::linkTo(PadPtr& pad)
{
    auto isCanLink = gst_pad_can_link(_padSrc.get(),pad.get() );

    makePad(PadType::PAD_SRC);
    try
    {
        if (isCanLink)
        {
            if ( gst_pad_link(_padSrc.get(),pad.get()) == GST_PAD_LINK_OK )
            {
                return true;
            }
            throw std::runtime_error("Link "  + _name  + "->" + gst_pad_get_name(pad.get())+ " failed\n");
        }
        else
        {
            throw std::runtime_error("Cannot Link" + _name  + "->" + gst_pad_get_name(pad.get()));
        }
    }catch (std::exception& e)
    {
        std::cerr << "Exeption. What : \n\t" << e.what();
    }
}

std::unique_ptr<GstPad,decltype(&gst_object_unref)>& BinBase::getSrcPad()
{
    return _padSrc;
}

std::unique_ptr<GstPad,decltype(&gst_object_unref)>& BinBase::getSinkPad()
{
    return _padSink;
}

 bool BinBase::linkTo(std::unique_ptr<IBin>& bin)
{
    auto& Padsink =  bin->getSinkPad();
   try
   {
       linkTo(Padsink);
   }
   catch (std::exception& e)
   {
       std::cerr << "Exeption. What : Cannot link " <<  _name << "->" << bin->_name <<". What : \n\t" << e.what();
   }
}


bool BinBase::linkTo(Element& element)
{
    PadPtr Padsink =  PadPtr(gst_element_get_static_pad(element->toGstData().get(),"sink"),&gst_object_unref);
    try
    {
        if (linkTo(Padsink))
        {
            return true;
        }
        throw  std::runtime_error("Link "  + _name  + "->" + element->getName() + " failed\n");
    }
    catch (std::exception& e)
    {
        std::cerr << "Exeption. What : Cannot link " <<  _name << "->" << element->getName() <<". What : \n\t" << e.what();
    }

}

std::unique_ptr<IElement>& BinBase::front()
{
    return _items.front();
}
std::unique_ptr<IElement>& BinBase::back()
{
    return _items.back();
}

bool BinBase::makePad(PadType type)
{
    switch (type)
    {
        case PadType::PAD_SRC:
            if (!_padSrc)
            {
                _padSrc = PadPtr(gst_ghost_pad_new("src",
                                                   gst_element_get_static_pad (back()->toGstData().get(), "src")),&gst_object_unref);
            }
            return  gst_element_add_pad ((GstElement*)_bin.get(),
                                         _padSrc.get());
        case PadType::PAD_SINK:
            if (!_padSink)
            {
                _padSink = PadPtr(gst_ghost_pad_new("sink",gst_element_get_static_pad (front()->toGstData().get(), "sink")),&gst_object_unref);
            }
            return gst_element_add_pad ((GstElement*)_bin.get(), _padSink.get());
    }

}

bool BinBase::link()
{
    bool isLinked = false;
    for (auto i = _items.begin(); i != _items.end() - 1 ; ++i)
    {
        isLinked = gst_element_link_many(i->get()->toGstData().get(),(i+1)->get()->toGstData().get(), nullptr);
    }
    makePad(PadType::PAD_SINK);
    return isLinked;
}

bool BinBase::add(std::unique_ptr<IElement>&& element)
{
    _items.push_back(std::move(element));
    return gst_bin_add(GST_BIN(_bin.get()),back()->toGstData().get());
}

std::unique_ptr<GstElement, decltype(&gst_object_unref)> BinBase::toGstElement(BinPtr&& bin)
{
    std::unique_ptr<GstElement, decltype(&gst_object_unref)> gstElement = std::unique_ptr<GstElement, decltype(&gst_object_unref)>
            (GST_ELEMENT(bin.get()),&g_object_unref);
    return gstElement;
}

BinPtr& BinBase::toGstData()
{
    return _bin;
}



bool BinBase::add(std::unique_ptr<IBin>&& bin)
{

    Element element = ElementBase::makeFrom(std::move(BinBase::toGstElement(std::move(bin->toGstData()))));
    _items.push_back(std::move(element));
    return gst_bin_add(GST_BIN(_bin.get()),back()->toGstData().get());
}

void BinBase::add_to_pipeline(GstElement* pipeline,const std::unique_ptr<IBin>& bin)
{
    gst_bin_add (GST_BIN (pipeline), GST_ELEMENT(bin->_bin.get()));
}