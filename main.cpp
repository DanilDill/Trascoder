﻿#include <Transcoder/Transcoder.h>
#include <include/Parameters.h>

int main(int argc, char* argv[])
{
    Parameters parameters;
    parameters.videoHeight = 720;
    parameters.videoWidth = 1080;
    parameters.framerate = 25;
    parameters.aspectRatioY = 1;
    parameters.aspectRatioX = 1;
    parameters.decodeBy = "avdec_h264";
    parameters.encodeBy = "nvh264enc";
    parameters.url = "http://extms01.aile.tv/media_detskij_mir_teleklub?sid=coder_65";
    parameters.outputPath = "/home/danil/hlstest";
    parameters.segmentSecs = 5;
    parameters.bframes = 2;
    parameters.video_bitrate = 2000;
    gst_init(&argc, &argv);
    Transcoder gst = Transcoder( argc, argv, parameters );
    if (gst.GstGetTerminate())
    {
        return -1;
    }

    gst.prepare();

    if (gst.GstGetTerminate())
    {
        return -1;
    }

    gst.startPlaying();
    return 0;
}
